const fs = require("fs");
const parser = require("xml2json");
const mysql = require("mysql");
const kanjidic = [];

class Readings {
  constructor(ja_on, ja_kun) {
    this.ja_on = [...ja_on];
    this.ja_kun = [...ja_kun];
  }
}

class Kanji {
  constructor(literal, grade, meanings, readings) {
    this.literal = literal;
    this.grade = grade;
    this.meanings = [...meanings];
    this.readings = { ...readings };
  }
}

const xmlParse = () => {
  return new Promise((resolve, reject) => {
    fs.readFile("kanjidic2.xml", function (err, data) {
      let xml = parser.toJson(data);
      resolve(xml);
    });
  });
};

const main = async (callback) => {
  let json = JSON.parse(await xmlParse());

  for (let character of json.kanjidic2.character) {
    if (!!character.reading_meaning) {
      let meanings = [];
      let readings_on = [];
      let readings_kun = [];

      if (!!character.reading_meaning.rmgroup.meaning) {
        if (Array.isArray(character.reading_meaning.rmgroup.meaning)) {
          for (let meaning of character.reading_meaning.rmgroup.meaning) {
            if (typeof meaning != "object") meanings.push(meaning);
          }
        } else {
          if (typeof character.reading_meaning.rmgroup.meaning != "object")
            meanings.push(character.reading_meaning.rmgroup.meaning);
        }
      }

      if (!!character.reading_meaning.rmgroup.reading) {
        if (Array.isArray(character.reading_meaning.rmgroup.reading)) {
          for (let reading of character.reading_meaning.rmgroup.reading) {
            if (reading.r_type == "ja_on") readings_on.push(reading.$t);
            if (reading.r_type == "ja_kun") {
              if (reading.$t.includes('.')) {
                let read = reading.$t.split('.');
                readings_kun.push(read[0]);
              } else {
                readings_kun.push(reading.$t);
              }
            }
          }
        } else {
          if (character.reading_meaning.rmgroup.reading.r_type == "ja_on")
            readings_on.push(character.reading_meaning.rmgroup.reading.$t);
          if (character.reading_meaning.rmgroup.reading.r_type == "ja_kun")
            readings_kun.push(character.reading_meaning.rmgroup.reading.$t);
        }
      }

      kanjidic.push(
        new Kanji(
          character.literal,
          !!character.misc.grade ? parseInt(character.misc.grade) : 0,
          meanings,
          new Readings(readings_on, readings_kun)
        )
      );
    }
  }

  //   console.log(kanjidic);
  callback();
};

const insert = () => {
  const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "kanji-royale",
  });

  connection.connect(function (err) {
    if (err) throw err;
  });

  for (let kanji of kanjidic) {
    let meanings = "";
    let on_readings = "";
    let kun_readings = "";

    meanings = kanji.meanings.join(", ");

    for (let on_yomi of kanji.readings.ja_on) {
      on_readings += `${on_yomi}, `;
    }

    on_readings = on_readings.slice(0, -2);

    for (let kun_yomi of kanji.readings.ja_kun) {
      kun_readings += `${kun_yomi}, `;
    }

    kun_readings = kun_readings.slice(0, -2);

    // console.log(meanings);
    // console.log(on_readings);
    // console.log(kun_readings);

    let query = "INSERT INTO `kanji` VALUES (0, '" + kanji.literal + "', " + kanji.grade + ", '" + meanings + "', '" + on_readings + "', '"+ kun_readings + "')";

    connection.query(query, function (err, results) {
        if (!!results) console.log(results.insertId);
    });
  }

  connection.end();
};

main(insert);
