### KANJIDIC-MYSQL-PARSER

***

### DESCRIPTION

App created in purpose of loading kanjidic2 XML file into MySQL database. 
Needed for my KANJI-ROYALE app (look at references).

### ATRIBUTES

!! Only few atributes from XML file were choosen.

Converts xml file to MySQL table in format:
id  |  literal  |  grade  |  meanings  |  on_readings  |  kun_readings

### REFERENCES

KANJIDIC:
http://www.edrdg.org/wiki/index.php/KANJIDIC_Project

KANJI-ROYALE:
https://gitlab.com/niejakub02/kanji-royale
